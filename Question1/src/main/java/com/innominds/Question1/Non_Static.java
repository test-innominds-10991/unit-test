package com.innominds.Question1;

public class Non_Static {

	static int staticval=0;
	int nonstaticval=0;
	
	public void increment() {
		staticval++;
		nonstaticval++;
	}
	
	public int printstat() {
	   return staticval;
		
	}
	
	public int printnonstat() {
		return nonstaticval;
	}
	
	public static void main(String[] as) {
		Non_Static obj1= new Non_Static();
		obj1.increment();
		System.out.println(obj1.printstat());
		System.out.println(obj1.printnonstat());
		
		Non_Static obj2= new Non_Static();
		obj2.increment();
		System.out.println(obj1.printstat());
		System.out.println(obj1.printnonstat());
		
		Non_Static obj3= new Non_Static();
		obj3.increment();
		System.out.println(obj1.printstat());
		System.out.println(obj1.printnonstat());
	}
}