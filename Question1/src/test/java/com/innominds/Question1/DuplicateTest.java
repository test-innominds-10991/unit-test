package com.innominds.Question1;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DuplicateTest 
	{
	
	@Test
	void testDuplicate() 
	{
		Duplicate d = new Duplicate();
		assertArrayEquals(new char[] {'t','e'}, d.duplicate1());
	}

}