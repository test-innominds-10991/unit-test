package com.innominds.Question1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Non_StaticTest {

	@Test
	public void test1() {
    Non_Static obj1=new Non_Static();
    obj1.increment();
    assertEquals(1,obj1.nonstaticval);

	}
	
	@Test
	public void test2() {
	Non_Static obj2=new Non_Static();
    obj2.increment();
    assertNotEquals(2,obj2.nonstaticval);

	}
	    
	}


