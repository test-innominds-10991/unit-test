package com.innominds.Question1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PriorThreadTest {

	PriorThread p=new PriorThread();
	PriorThread p1=new PriorThread();
	PriorThread p2=new PriorThread();
	PriorThread p3=new PriorThread();
	PriorThread p4=new PriorThread();
	
	
@Test
public void multithread() 
{
	
	p.setPriority(10);
	p1.setPriority(9);
	p2.setPriority(5);
	p3.setPriority(7);
	p4.setPriority(6);
	p.setName("1st Thread");
	p.setName("2nd Thread");
	p.setName("3rd Thread");
	p.setName("4th Thread");
	p.setName("5th Thread");
	p.start();
	assertTrue(p.isAlive());
	p1.start();
	System.out.println(Thread.currentThread());
	assertTrue(p1.isAlive());
	p2.start();
	assertTrue(p2.isAlive());
	p3.start();
	assertTrue(p3.isAlive());
	p4.start();
	assertTrue(p4.isAlive());

}


}
